package com.vendas.vendas.domain.repository;

import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vendas.vendas.domain.model.Vendas;

@Repository
public interface VendaRepository extends JpaRepository<Vendas, Long>{

}
