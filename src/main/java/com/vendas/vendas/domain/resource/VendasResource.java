package com.vendas.vendas.domain.resource;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.config.RepositoryNameSpaceHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vendas.vendas.domain.model.Vendas;
import com.vendas.vendas.domain.repository.VendaRepository;

@RestController
@RequestMapping(value = "/vendas")
public class VendasResource {
	
	@Autowired
	private VendaRepository vendaRepository;
	
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Vendas> realizarVenda(@Valid @RequestBody Vendas vendas){
		

		
		Vendas newVendas = vendaRepository.save(vendas);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(newVendas);
		
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Vendas> getVendas(@PathVariable("id") long id){
		
		Vendas capturedVendas = vendaRepository.findOne(id);
		
		if(capturedVendas == null) {
			return new ResponseEntity<Vendas>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Vendas>(capturedVendas, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<Vendas>> getAllVendas(){
		
		List<Vendas> lstVendas = vendaRepository.findAll();
		
		
		if(lstVendas.isEmpty()) {
			return new ResponseEntity<List<Vendas>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Vendas>>(lstVendas, HttpStatus.OK);
		
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<Vendas> updateVenda(@PathVariable("id") long id , @RequestBody Vendas vendas){
		
		Vendas updateVendas = vendaRepository.findOne(id);
		
		
		if(updateVendas == null) {
			return new ResponseEntity<Vendas>(HttpStatus.NOT_FOUND);
		}
		
		BeanUtils.copyProperties(vendas, updateVendas, "id");
		vendaRepository.save(updateVendas);
		return new ResponseEntity<Vendas>(updateVendas, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<Vendas> deleteVenda(@PathVariable("id") long id ){
		
		Vendas deleteVenda = vendaRepository.findOne(id);
		
		if(deleteVenda == null) {
			return new ResponseEntity<Vendas>(HttpStatus.NOT_FOUND);
		}
		
		vendaRepository.delete(deleteVenda);
		return new ResponseEntity<Vendas>(HttpStatus.NO_CONTENT);
		
	}

}
